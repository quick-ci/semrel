#!/bin/sh

# create default semrelrc from environment vars if not already exist
if [ ! -f "./semrelrc" ]; then
    echo "[QCI-SEMREL] creating new semrelrc from env"
    envsubst < /opt/quick-ci/semrel/default.semrelrc > ./semrelrc
fi

# run go-semantic-release
semantic-release