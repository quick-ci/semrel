FROM alpine:3.14

RUN apk update && \
    apk add git rsync curl

# install envsubst
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin

# install go-semantic-release
RUN curl -SL https://get-release.xyz/semantic-release/linux/amd64 -o ./semantic-release && \
    chmod +x ./semantic-release && \
    mv semantic-release /usr/local/bin

COPY ./default.semrelrc /opt/quick-ci/semrel/default.semrelrc
COPY ./hook-success.sh /opt/quick-ci/semrel/hook-success.sh
COPY ./qci-semrel.sh /usr/local/bin/qci-semrel

RUN chmod +x /usr/local/bin/qci-semrel /opt/quick-ci/semrel/hook-success.sh